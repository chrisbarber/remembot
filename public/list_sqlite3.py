from list_db import L2
import sqlite3

class ListSqlite3Adapter:
    FIELDS = ['id', 'n', 'a', 'b', 'prev', 'next', 'link']

    def init_db(self):
        conn = sqlite3.connect('sqlite.db')
        self.conn = conn
        self.conn.execute('''
            CREATE TABLE IF NOT EXISTS
            l (id INTEGER PRIMARY KEY, n REAL, a VARCHAR, b VARCHAR,
                prev INT, next INT, link INT)''')
        self.conn.execute('''
            CREATE TABLE IF NOT EXISTS
            h (id PRIMARY KEY, head INT)''')
        self.conn.execute('''
            CREATE TABLE IF NOT EXISTS
            t (id PRIMARY KEY, tail INT)''')
        self.conn.execute('''
            CREATE TABLE IF NOT EXISTS
            m (id PRIMARY KEY, metric INT)''')
        self.commit()

        self.head = (self.conn.execute('''SELECT head FROM h''').fetchone() or (None,))[0]
        self.tail = (self.conn.execute('''SELECT tail FROM t''').fetchone() or (None,))[0]
        self.metric = (self.conn.execute('''SELECT metric FROM m''').fetchone() or (None,))[0]
        if self.metric is None:
            self.update_metric(0.)

    def update_head(self, id):
        self.head = id
        self.conn.execute('''DELETE FROM h''')
        self.conn.execute('''INSERT INTO h (head) VALUES ({})'''.format(id or 'NULL'))

    def update_tail(self, id):
        self.tail = id
        self.conn.execute('''DELETE FROM t''')
        self.conn.execute('''INSERT INTO t (tail) VALUES ({})'''.format(id or 'NULL'))

    def update_metric(self, x):
        self.metric = x
        self.conn.execute('''DELETE FROM m''')
        self.conn.execute('''INSERT INTO m (metric) VALUES ({})'''.format(x or 'NULL'))

    def select_one(self, id):
        record = self.conn.execute(
            '''SELECT id, n, a, b, prev, next, link FROM l WHERE id = {}'''.format(id)).fetchone()
        return dict(zip(self.FIELDS, record))

    def format_null(self, x):
        return x if x is not None else 'NULL'

    def insert_record(self, record, cb):
        self.conn.execute('''INSERT INTO l ({}) VALUES ({})'''.format(
            ','.join(record.keys()),
            ','.join([("'{}'" if isinstance(x, str) else "{}").format(self.format_null(x)) for x in record.values()]),
        ))
        cb(self.conn.execute('''SELECT LAST_INSERT_ROWID()''').fetchone()[0])

    def update_record(self, record):
        self.conn.execute('''UPDATE l SET {} WHERE id = {}'''.format(
            ','.join(['{} = {}'.format(f, ("'{}'" if isinstance(v, str) else "{}").format(self.format_null(v))) for f, v in
                zip(record.keys(), record.values())]),
            record['id'],
        ))

    def delete(self, id):
        self.conn.execute('''DELETE FROM l WHERE id = {}'''.format(id))

    def commit(self):
        self.conn.commit()

    def __contains__(self, x):
        return 0<self.conn.execute('''SELECT COUNT(*) FROM l WHERE a = '{}' '''.format(x)).fetchone()[0]

    def __len__(self):
        return self.conn.execute('''SELECT COUNT(*) FROM l''').fetchone()[0]

    def __del__(self):
        self.conn.close()

class L2Sqlite3(ListSqlite3Adapter, L2):
    pass

