import math

def input_with_prefill(prompt, text):
    return (yield (prompt, text))

def input_without_prefill(prompt):
    return input_with_prefill(prompt, '')

def edit(b, x):
    delete = False
    while b.startswith('/') and not delete:
        if b == '/edit':
            A = yield from input_with_prefill('A ', x[1])
            x[1] = A
            b = yield from input_without_prefill(x[1] + '? ')
            if b == '/edit':
                B = yield from input_with_prefill('B ', x[2])
                x[2] = B
                b = yield from input_without_prefill(x[1] + '? ')
        elif b == '/delete':
            delete = True
            #break #brython issue?
        else:
            b = yield from input_without_prefill(x[1] + '? ')
    return (delete, b, x)

def loop(l):
    while True:
            #print(repr(l))
            #print(len(l))

            # TODO: mutex for pending writes, JS timer poll to advance loop when ready
            junk = yield from input_without_prefill('pause')

            if l.empty() or l.get_metric() > math.log2(len(l)):
                while True:
                    a = yield from input_without_prefill('a ')
                    if a and not a in l:
                        break
                b = yield from input_without_prefill('b ')
                #print(None, (0, a, b))
                l.insert(None, (0, a, b))
            else:
                x = list(l.pop(0))
                b = yield from input_without_prefill(x[1] + '? ')
                # could do tuple packing on same line as `yield from` here and below, but:
                # https://github.com/brython-dev/brython/issues/1035
                t = yield from edit(b, x)
                delete, b, x = t
                # delete logic here and below could be `continue` but:
                # https://github.com/brython-dev/brython/issues/1034
                if not delete:
                    step = -x[0] + math.log2(len(l)*2**(x[0]+1)/(len(l)+1)+2)
                    if b == x[2]:
                        n = x[0]+step
                    elif b == '-':
                        while True:
                            b = yield from input_without_prefill('(y/n) ' + x[2] + '? ')
                            t = yield from edit(b, x)
                            delete, b, x = t
                            if delete:
                                break
                            if b == 'y':
                                n = x[0]+step
                                break
                            elif b == 'n':
                                n = max(0, x[0]-step)
                                break
                    else:
                        while True:
                            b = yield from input_without_prefill('!!! ' + x[2] + '? ')
                            t = yield from edit(b, x)
                            delete, b, x = t
                            if delete:
                                break
                            if b == x[2]:
                                break
                        n = max(0, x[0]-step)
                    if not delete:
                        l.insert(None, (n, x[1], x[2]))
