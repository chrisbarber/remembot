import math

class L2:
    def __init__(self):
        self.init_db()

    def add_to_metric(self, x):
        N = len(self)
        self.update_metric(self.metric-self.metric/N+x/N)

    def remove_from_metric(self, x):
        N = len(self)
        if 0==N:
            self.update_metric(0.)
        else:
            self.update_metric(self.metric+self.metric/N-x/N)

    def get_metric(self):
        return self.metric

    def empty(self):
        return 0==len(self)

    def peek(self):
        if self.head:
            return self.select_one(self.head)
        else:
            return None

    # TODO: callback style of self.insert_record usages can be
    # rewritten after https://github.com/brython-dev/brython/issues/210

    def append(self, x):
        last_link = None
        if math.log2(len(self)+1)%1 == 0.:
            link = self.head
            while link is not None:
                last_link = link
                link = self.select_one(link)['link']
        def cb(new_id):
            if self.tail:
                self.update_record({'id': self.tail, 'next': new_id})
            self.update_tail(new_id)
            if last_link:
                self.update_record({'id': last_link, 'link': self.tail})
            if not self.head:
                self.update_head(self.tail)
            self.add_to_metric(x[0])
            self.commit()
        self.insert_record(
            {'n': x[0], 'a': x[1], 'b': x[2], 'prev': self.tail, 'next': None, 'link': None},
            cb,
        )

    def pop(self, i):
        assert(i==0)
        if not self.head:
            return None
        id = self.head
        while id is not None:
            next_, link = (self.select_one(id)[k] for k in ('next', 'link'))
            if not link:
                break
            link_id, link_next, link_link = (self.select_one(link)[k] for k in ('id', 'next', 'link'))
            self.update_record({'id': id, 'link': link_next})
            self.update_record({'id': link_id, 'link': None})
            if link_next:
                self.update_record({'id': link_next, 'link': link_link})
            id = link_next
        x = tuple(self.peek()[k] for k in ('n', 'a', 'b'))
        next_, link = (self.select_one(self.head)[k] for k in ('next', 'link'))
        self.delete(self.head)
        self.update_head(next_)
        if self.head:
            self.update_record({'id': self.head, 'prev': None, 'link': link})
        else:
            self.update_tail(None)
        self.remove_from_metric(x[0])
        self.commit()
        return x

    def insert(self, _, x):
        n = int(round(x[0]))
        last_id = None
        id = self.head
        if not id:
            self.append(x)
            return
        for _ in range(n):
            last_id = id
            id = self.select_one(id)['link']
            if not id:
                self.append(x)
                return
        prev, link = (self.select_one(id)[k] for k in ('prev', 'link'))
        # TODO: issue with `id` and nested function binding
        id_ = id
        def cb(new_id):
            id = id_
            self.update_record({'id': id, 'prev': new_id, 'link': None})
            if last_id:
                self.update_record({'id': last_id, 'link': new_id})
            if prev:
                self.update_record({'id': prev, 'next': new_id})
            else:
                self.update_head(new_id)
            id = new_id
            while id is not None:
                link = self.select_one(id)['link']
                if not link:
                    if math.log2(len(self))%1 == 0.:
                        self.update_record({'id': id, 'link': self.tail})
                    break
                link_prev, link_link = (self.select_one(link)[k] for k in ('prev', 'link'))
                self.update_record({'id': id, 'link': link_prev})
                self.update_record({'id': link, 'link': None})
                self.update_record({'id': link_prev, 'link': link_link})
                id = link_prev
            self.add_to_metric(x[0])
            self.commit()
        self.insert_record(
            {'n': x[0], 'a': x[1], 'b': x[2], 'prev': prev, 'next': id, 'link': link},
            cb,
        )

    def __repr__(self):
        return super().__repr__() + "\n" + str(self)

    def __str__(self):
        s = ''
        if not self.head:
            return '0 rows'
        else:
            id = self.head
            while id:
                row = self.select_one(id)
                import json
                s += json.dumps(row)
                #s += str(row)
                id = row['next']
                if id:
                    s += "\n"
        return s
