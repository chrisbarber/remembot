import readline
from list_ref import L1
from list_sqlite3 import L2Sqlite3
from loop import loop
import os

def readline_input_prefill(prompt, text):
    def hook():
        readline.insert_text(text)
        readline.redisplay()
    readline.set_pre_input_hook(hook)
    result = input(prompt)
    readline.set_pre_input_hook()
    return result

#l = L1()
l = L2Sqlite3()

loop_gen = loop(l)
prompt, text = next(loop_gen)

while True:
    os.system('clear')
    prompt, text = loop_gen.send(readline_input_prefill(prompt, text))
