class L1:
    def __init__(self):
        self.l = []
        self.metric = 0.

    def empty(self):
        return not self.l

    def peek(self):
        return self.l[0]

    def append(self, x, skip_metric=False):
        self.l.append(x)
        if not skip_metric:
            self.add_to_metric(x[0])

    def pop(self, i):
        assert(i==0)
        x=self.l.pop(0)
        self.remove_from_metric(x[0])
        return x

    def insert(self, _, x, skip_metric=False):
        i = int(round(2**x[0]-1))
        self.l.insert(i, x)
        if not skip_metric:
            self.add_to_metric(x[0])

    def add_to_metric(self, x):
        N = len(self)
        self.metric = self.metric-self.metric/N+x/N

    def remove_from_metric(self, x):
        N = len(self)
        if 0.==N:
            self.metric = 0.
        else:
            self.metric = self.metric+self.metric/N-x/N

    def get_metric(self):
        return self.metric

    def __len__(self):
        return len(self.l)

    def __repr__(self):
        return super().__repr__() + "\n" + str(self)

    def __str__(self):
        return "\n".join(map(str, self.l))

    def __contains__(self, x):
        return x in map(lambda x: x[1], self.l)
