import math
import readline

class L1:
    def __init__(self):
        self.l = []
        self.metric = 0.

    def empty(self):
        return not self.l

    def peek(self):
        return self.l[0]

    def append(self, x):
        self.l.append(x)
        self.add_to_metric(x[0])

    def pop(self, i):
        assert(i==0)
        x=self.l.pop(0)
        self.remove_from_metric(x[0])
        return x

    def insert(self, _, x):
        i = int(round(2**x[0]-1))
        self.l.insert(i, x)
        self.add_to_metric(x[0])

    def add_to_metric(self, x):
        N = len(self)
        self.metric = self.metric-self.metric/N+x/N

    def remove_from_metric(self, x):
        N = len(self)
        if 0.==N:
            self.metric = 0.
        else:
            self.metric = self.metric+self.metric/N-x/N

    def get_metric(self):
        return self.metric

    def __len__(self):
        return len(self.l)

    def __repr__(self):
        return super().__repr__() + "\n" + str(self)

    def __str__(self):
        return "\n".join(map(str, self.l))

    def __contains__(self, x):
        return x in map(lambda x: x[1], self.l)

l = L1()

def input_with_prefill(prompt, text):
    return (yield (prompt, text))
    
def input_without_prefill(prompt):
    return (yield from input_with_prefill(prompt, ''))
    

def edit():
    global delete, b, x
    while not b:
        A = yield from input_with_prefill('A ', x[1])
        if not A:
            delete = True
            break
        x[1] = A
        b = yield from input_without_prefill('? ')
        if not b:
            B = yield from input_with_prefill('B ', x[2])
            if not B:
                delete = True
                break
            x[2] = B
            b = yield from input_without_prefill('? ')

def loop():
    global delete, b, x
    while True:
            print(repr(l))
            #print(len(l))

            if l.empty() or l.get_metric() > math.log2(len(l)):
                while True:
                    a = yield from input_without_prefill('a ')
                    if not a in l:
                        break
                b = yield from input_without_prefill('b ')
                l.insert(None, (0, a, b))
            else:
                x = list(l.pop(0))
                b = yield from input_without_prefill(x[1] + '? ')
                delete = False
                yield from edit()
                if delete:
                    continue

                step = -x[0] + math.log2(len(l)*2**(x[0]+1)/(len(l)+1)+2)
                if b == x[2]:
                    n = x[0]+step
                else:
                    while True:
                        b = yield from input_without_prefill('!!! ' + x[2] + '? ')
                        delete = False
                        yield from edit()
                        if delete:
                            continue
                        if b == x[2]:
                            break
                    n = max(0, x[0]-step)
                l.insert(None, (n, x[1], x[2]))

loop_gen = loop()
prompt, text = next(loop_gen)

def readline_input_with_prefill(prompt, text):
    def hook():
        readline.insert_text(text)
        readline.redisplay()
    readline.set_pre_input_hook(hook)
    result = input(prompt)
    readline.set_pre_input_hook()
    return result

while True:
    #prompt, text = loop_gen.send(input(prompt, text))
    prompt, text = loop_gen.send(readline_input_with_prefill(prompt, text))
